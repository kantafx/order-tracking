import pika
import redis
import json
import requests
from apscheduler.schedulers.background import BackgroundScheduler

API_KEY = "6ffe59ec-ee1a-4760-9449-bd8586708e01"
ORDER_CREATED_QUEUE = "order-created-queue"
ORDER_CREATED_EXCHANGE = "order-created-exchange"
ORDER_EXECUTED_QUEUE = "order-executed-queue"
ORDER_EXECUTED_EXCHANGE = "order-executed-exchange"
ORDER_EXECUTED_ROUTING_KEY = "order-executed-routing-key"

ENDPOINT = "http://exchange.matraining.com"

redis_client = redis.Redis(
    host='redis-13585.c8.us-east-1-2.ec2.cloud.redislabs.com',
    port=13585,
    password='EFBhBPyrgz3FpPS4BSCe7pmRJOANdHZ2')

redis_client.ping()


def get_order_executions(endpoint, order_id):
    response = requests.get(f"{endpoint}/{API_KEY}/order/{order_id}")
    data = response.json()
    if data["executions"] is None or len(data["executions"]) < 1:
        return None
    return data


def check_for_executions():
    raw_order_data = redis_client.lrange("orders", 0, -1)
    order_data = [json.loads(x.decode("utf-8")) for x in raw_order_data]
    for x in order_data:
        order_id = x["order_id"]
        order_data = get_order_executions(ENDPOINT, order_id)
        channel.basic_publish(exchange=ORDER_EXECUTED_EXCHANGE, routing_key=ORDER_EXECUTED_ROUTING_KEY,
                              body=json.dumps(order_data))


def start_scheduler():
    scheduler = BackgroundScheduler()
    scheduler.add_job(check_for_executions, trigger="interval", seconds=5)
    scheduler.start()


def process_command(payload):
    command = payload['command']
    match command:
        case "add":
            order = payload['body']
            redis_client.rpush("orders", json.dumps(order))
            print(f"added {order}")
        case "list_all":
            print(redis_client.lrange("orders", 0, -1))
        case "empty":
            length = redis_client.llen("orders")
            redis_client.lpop("orders", count=length)


def callback(ch, method, properties, body):
    json_body = json.loads(body)
    try:
        process_command(json_body)
        ch.basic_ack(delivery_tag=method.delivery_tag)
    except Exception as err:
        print("An error occurred")
        print(err)


start_scheduler()

connection_parameters = pika.ConnectionParameters(host="moose.rmq.cloudamqp.com",
                                                  virtual_host="vlywzgxb",
                                                  credentials=pika.PlainCredentials(username="vlywzgxb",
                                                                                    password="fReiEFJtGLlnMTauLRlyG0rD61qJqs4h"))

connection = pika.BlockingConnection(parameters=connection_parameters)

channel = connection.channel()

channel.exchange_declare(exchange=ORDER_CREATED_EXCHANGE, durable=True)
channel.exchange_declare(exchange=ORDER_EXECUTED_EXCHANGE, durable=True)

channel.queue_declare(queue=ORDER_CREATED_QUEUE, durable=True)
channel.queue_declare(queue=ORDER_EXECUTED_QUEUE, durable=True)

channel.basic_consume(queue=ORDER_CREATED_QUEUE, on_message_callback=callback)

channel.start_consuming()


